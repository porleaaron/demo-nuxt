module.exports = {
    theme: {
        colors: {
            transparent: 'transparent',
            current: 'currentColor',
            'color0': '#ffffff',
            'color1': '#E6FFFA',
            'color2': '#EBF4FF',
            'color3': '#F7FAFC',
            'color4': '#718096',
            'color5': '#4A5568',
            'color6': '#2D3748',
            'color7': '#CBD5E0',
            'color8': '#81E6D9',
            'color9': '#319795',
            'color10': '#00000029',
            'color11': '#3182CE',
            'color12': '#00000033',
            'color13': '#707070',
        },
        boxShadow: {
            '3xl': '0 3px 3px #00000029',
            'top': '0 -1px 3px #00000029',
          }
    },
  }